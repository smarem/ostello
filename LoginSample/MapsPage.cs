﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Diagnostics;
using Plugin.Geolocator;
using System.Timers;
using System.IO;
using System.Collections.Generic;
using Plugin.ExternalMaps;
using LoginSample.CustomRenderers;

namespace LoginSample
{
    public class MapsPage:ContentPage

    {
        PolylineMap map;
        //   Position position;
        Position positions;
        Label latitude, logitude;
        private string address;

        public MapsPage()
        {
            GetMap();

            map.MoveToRegion(MapSpan.FromCenterAndRadius(positions, Distance.FromMiles(2)));
        }

        public  void GetMap(){
            map = new PolylineMap
            {
                IsShowingUser = true,
                HeightRequest = 200,
                WidthRequest = 200,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            var slider = new Slider(1, 18, 1);
            slider.ValueChanged += (sender, e) =>
            {
                var zoomLevel = e.NewValue;
                var latLongDegrees = 360 / Math.Pow(2, zoomLevel);
                Debug.WriteLine(zoomLevel + "->" + latLongDegrees);
                if (map.VisibleRegion != null)

                    map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latLongDegrees, latLongDegrees));

            };

            var street = new Button
            {
                Text = "Street"
            };
            var hybrid = new Button
            {
                Text = "Hybrid"
            };
            var satellite = new Button
            {
                Text = "Satellite"
            };
            var chooselocation = new Button
            {
                Text = "ChooseLocation",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand

            };
            latitude = new Label
            {
                Text = "lati",
                TextColor = Color.Blue
            };
            logitude = new Label
            {
                Text = "longi",
                TextColor = Color.Blue

            };


            street.Clicked += HandleClicked;
            hybrid.Clicked += HandleClicked;
            satellite.Clicked += HandleClicked;
            chooselocation.Clicked += HandleClicked;
            var segments = new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Spacing = 30,
                Orientation = StackOrientation.Horizontal,
                Children ={
                    street,
                    hybrid,
                    satellite
                }
            };
            var labels = new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Spacing = 10,
                Orientation = StackOrientation.Vertical,
                Children ={
                    latitude,
                    logitude
                }
            };
            var stack = new StackLayout
            {
                Spacing = 0
            };
            var scroll = new ScrollView();


            stack.Children.Add(map);
            stack.Children.Add(labels);
            stack.Children.Add(chooselocation);
            stack.Children.Add(segments);
            stack.Children.Add(slider);
            scroll.Content = stack;

            Content = scroll;
            map.PropertyChanged += (sender, e) =>
            {
                Debug.WriteLine(e.PropertyName + " just changed!");
                if (e.PropertyName == "VisibleRegion" && map.VisibleRegion != null) CalculateBoundingCoordinates(map.VisibleRegion);

            };

        }





        public  MapsPage(string text)
        {
            this.address = text;

           // Getpositions();
            map = new PolylineMap
            {
                IsShowingUser = true,
                HeightRequest = 500,
                WidthRequest = 300,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            var slider = new Slider(1, 18, 1);
            slider.ValueChanged += (sender, e) =>
            {
                var zoomLevel = e.NewValue;
                var latLongDegrees = 360 / Math.Pow(2, zoomLevel);
                Debug.WriteLine(zoomLevel + "->" + latLongDegrees);
                if (map.VisibleRegion != null)

                    map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latLongDegrees, latLongDegrees));

            };
            var button = new Button();
            button.Text = "kjlakj";
            button.TextColor = Color.Blue;
            button.Clicked
                  += (sender, e) =>
                  {

                      map.RouteCoordinates.Add(new Position(37.785559, -122.396728));
                      map.RouteCoordinates.Add(new Position(37.780624, -122.390541));
                      map.RouteCoordinates.Add(new Position(37.777113, -122.394983));
                      map.RouteCoordinates.Add(new Position(37.776831, -122.394627));

                      map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37.79752, -122.40183), Distance.FromMiles(1.0)));

                  };
          
            var stack = new StackLayout();
            stack.Children.Add(map);
            stack.Children.Add(button);
            stack.Children.Add(slider);
            Content = stack;
        }

        void HandleClicked(object sender, EventArgs e)
        {
            var b = sender as Button;
            switch (b.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;
                case "Hybrid":
                    map.MapType = MapType.Hybrid;
                    break;
                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;
                case "ChooseLocation":
                    CurrentLocation();
                    break;
            }
        }
        public async void CurrentLocation()
        {
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 20;
                var position = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(10000));
                latitude.Text = position.Latitude.ToString();
                logitude.Text = position.Latitude.ToString();

                map.MoveToRegion(
                  MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude)
                  , Distance.FromMiles(1)));
            }

        }

        public   async void   Getpositions(){


            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 20;
            var positionss = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(10000));
            var po = new Position(positionss.Latitude, positionss.Longitude);
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = po,
                Label = "custom pin",
                Address = "custom detail info"
            };
            map.Pins.Add(pin);
            //map.MoveToRegion(
            //  MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(positionss.Latitude, positionss.Longitude)
            //  , Distance.FromMiles(1)));
            Geocoder gc = new Geocoder();
            IEnumerable<Position> result =
                await gc.GetPositionsForAddressAsync(address);
           
            foreach (Position pos in result)
            {
                System.Diagnostics.Debug.WriteLine("Lat: {0}, Lng: {1}", pos.Latitude, pos.Longitude);
               
                positions = new Position(pos.Latitude, pos.Longitude);

                //await CrossExternalMaps.Current.NavigateTo("New Location", pos.Latitude, pos.Longitude,Plugin.ExternalMaps.Abstractions.NavigationType.Default);

            }

          
        }
        static void CalculateBoundingCoordinates(MapSpan region)
        {
            // WARNING: I haven't tested the correctness of this exhaustively!  
            var center = region.Center;
            var halfheightDegrees = region.LatitudeDegrees / 2;
            var halfwidthDegrees = region.LongitudeDegrees / 2;
            var left = center.Longitude - halfwidthDegrees;
            var right = center.Longitude + halfwidthDegrees;
            var top = center.Latitude + halfheightDegrees;
            var bottom = center.Latitude - halfheightDegrees;
            // Adjust for Internation Date Line (+/- 180 degrees longitude)  
            if (left < -180) left = 180 + (180 + left);
            if (right > 180) right = (right - 180) - 180;
            // I don't wrap around north or south; I don't think the map control allows this anyway  
            Debug.WriteLine("Bounding box:");
            Debug.WriteLine(" " + top);
            Debug.WriteLine(" " + left + " " + right);
            Debug.WriteLine(" " + bottom);
        }
      

    }
}
