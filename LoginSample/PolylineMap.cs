﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;
using System.Diagnostics;
using LoginSample.ViewModel;
using System.ComponentModel;

namespace LoginSample.CustomRenderers
{
    public class PolylineMap : Map
    {
        public static readonly BindableProperty RouteCoordinatesProperty = BindableProperty.Create(nameof(RouteCoordinates), typeof(List<Position>), typeof(PolylineMap), null);

        public List<Position> RouteCoordinates 
        {
            get
            {
                return (List<Position>)GetValue(RouteCoordinatesProperty);
            }
            set
            {
                SetValue(RouteCoordinatesProperty, value);
            }
        }

        public static readonly BindableProperty RoutePinsProperty = BindableProperty.Create(nameof(RoutePins), typeof(List<Pin>), typeof(PolylineMap), null);
        public List<Pin> RoutePins 
        { 
            get
            {
                return (List<Pin>)GetValue(RoutePinsProperty);
            }
            set
            {
                SetValue(RoutePinsProperty, value);
            }
        }

        public static readonly BindableProperty IsCoordinatesAddedProperty = BindableProperty.Create(nameof(IsCoordinatesAdded), typeof(bool), typeof(PolylineMap), false);
        public bool IsCoordinatesAdded
        {
            get
            {
                return (bool)GetValue(IsCoordinatesAddedProperty);
            }
            set
            {
                SetValue(IsCoordinatesAddedProperty, value);
            }
        }

        public static readonly BindableProperty IsPinsAddedProperty = BindableProperty.Create(nameof(IsPinsAdded), typeof(bool), typeof(PolylineMap), false);
        public bool IsPinsAdded
        {
            get
            {
                return (bool)GetValue(IsPinsAddedProperty);
            }
            set
            {
                SetValue(IsPinsAddedProperty, value);
            }
        }

        public PolylineMap()
        {
            RouteCoordinates = new List<Position>();
            RoutePins = new List<Pin>();
        }
    }
}

