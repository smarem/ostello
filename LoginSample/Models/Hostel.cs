﻿using System.Collections.Generic;
using System;
using LoginSample;
using SQLite;

namespace LoginSample
{
    public class Hostel : BaseViewModel
    {
        //public string _image;
        //public string Image
        //{
        //    get
        //    {
        //        return _image;
        //    }
        //    set
        //    {
        //        _image = value;
        //        RaisePropertyChanged("Image");
        //    }
        //}
        //private string _name;
        //public string Name
        //{
        //    get
        //    {
        //        return _name;
        //    }
        //    set
        //    {
        //        _name = value;
        //        RaisePropertyChanged("Name");
        //    }
        //}

        //private string number;
        //public string Number
        //{
        //    get
        //    {
        //        return number;
        //    }
        //    set
        //    {
        //        number = value;
        //        RaisePropertyChanged("Designation");
        //    }
        //}
        public Hostel()
        {

        }
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Image
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Number
        {
            get;
            set;
        }
        public string Location
        {
            get;
            set;
        }


    }
}