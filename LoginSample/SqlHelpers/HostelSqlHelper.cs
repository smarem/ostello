﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PCLStorage;
using SQLite;

namespace LoginSample.SqlHelpers
{
    public class HostelSqlHelper
    {
        static Object locker = new object();
        SQLiteConnection database;
        public HostelSqlHelper()
        {
            database = GetConnection();
            database.CreateTable<Hostel>();
        }

        public  SQLite.SQLiteConnection GetConnection(){
            SQLiteConnection sQLiteConnection;
            var sqliteFileName = "Hostels.db3";
            IFolder folder = FileSystem.Current.LocalStorage;
            string pathof = PortablePath.Combine(folder.Path.ToString(), sqliteFileName);
            sQLiteConnection = new SQLiteConnection(pathof);
            return sQLiteConnection;

        }

        public IEnumerable<Hostel> GetHostels(){
            lock(locker){
                return (from i in database.Table<Hostel>() select i).ToList();
            }

        }
        public Hostel GetHostel(int i){
            return database.Get<Hostel>(i);

        }
        public int SaveHostel(Hostel hostel){
            lock(locker){
                if (hostel.ID!=0){
                    database.Update(hostel);
                    return hostel.ID;

                }
                else{
                    return database.Insert(hostel);
                }
            }
        }


        public void DeleteHostel(Hostel i){
            lock(locker){
                database.Delete(i);
            }

        }
    }
}
