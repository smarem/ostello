﻿using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;
using PCLStorage;
namespace LoginSample
{
    public class SqlHelper
    {
        static object locker = new object();
        SQLiteConnection database;
        public SqlHelper()
        {
            database = GetConnection();
            database.CreateTable<RegEntity>();
           
        }
        public SQLite.SQLiteConnection GetConnection(){
            SQLiteConnection sQLiteConnection;
            var sqliteFileNmae = "User.db3";
            IFolder folder = FileSystem.Current.LocalStorage;
            string path = PortablePath.Combine(folder.Path.ToString(), sqliteFileNmae);
            sQLiteConnection = new SQLite.SQLiteConnection(path);
            return sQLiteConnection;
        }

        public IEnumerable<RegEntity> GetItems()
        {
            lock (locker)
            {
                
                return (from i in database.Table<RegEntity>() select i).ToList();
                
            }
        }
        public RegEntity GetItem(string userName){
            lock(locker){
                return database.Table<RegEntity>().FirstOrDefault(x => x.Username == userName);
            }
        }
        public RegEntity GetItem(string username ,string password){
            lock(locker){
                return database.Table<RegEntity>().FirstOrDefault(x => x.Username == username && x.Password == password);

            }
        }
        public int SaveItem(RegEntity item){
            lock(locker){
                if(item.ID!=0){
                    database.Update(item);
                    return item.ID;

                }
                else{
                    return database.Insert(item);

                }
            }
        }


        public int DeleteItem(int id){
            lock(locker){
                return database.Delete<Hostel>(id);
            }
        }


    }


}
