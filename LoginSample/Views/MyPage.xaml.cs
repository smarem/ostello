﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using LoginSample.ViewModel;
using Xamarin.Forms;
using LoginSample.SqlHelpers;
using LoginSample.Views;

namespace LoginSample
{
    public partial class MyPage : ContentPage
    {
        //void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        //{
        //    Hostel hostel = (Hostel)e.SelectedItem;
        //    var index = hostel.ID;
        //    Navigation.PushAsync(new ViewHostel(index));
        //}

        public List<Hostel> HostelList;
        public MyPage(){
            InitializeComponent();
           
            MyPageViewModel viewmodel = new MyPageViewModel();
            viewmodel.navigation = this.Navigation;
            BindingContext = viewmodel;
            
            //HostelSqlHelper hostelSqlHelper = new HostelSqlHelper();

            //// HostelList = new ObservableCollection<Hostel>();
            //HostelList = (System.Collections.Generic.List<LoginSample.Hostel>)hostelSqlHelper.GetHostels();
         
            //listviewhostel.ItemsSource = HostelList;
        }

        //void Handle_Clicked(object sender, System.EventArgs e)
        //{
        //    Navigation.PushAsync(new CreateHostel());
        //}

        //public ICommand createHostelSelected
        //{
        //    get
        //    {
        //        return new Command(createHostelSelectedCommand);
        //    }

        //}
        //public void createHostelSelectedCommand()
        //{
        //    Navigation.PushAsync(new CreateHostel(this));
        //}
    }
}
