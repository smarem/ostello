﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoginSample.ViewModel;
using Xamarin.Forms;

namespace LoginSample
{
    public partial class MainPage : ContentPage
    {
       
        public Boolean ok;
       
       
        List<String> userdetails = new List<string>();
        public MainPage()
        {
            InitializeComponent();
           

            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (s, e) =>
            {
                Navigation.PushAsync(new MapsPage());

            };
            createAccount.GestureRecognizers.Add(tap);
          
        }
      

        async void Handle_Clicked(object sender, System.EventArgs e)

        {
            if (username.Text!=null && password.Text!=null){
                SqlHelper sqlHelper = new SqlHelper();

                RegEntity userdetail = sqlHelper.GetItem(username.Text, password.Text);

                if(username.Text!=null)
                {
                act.IsVisible = true;
                act.IsRunning = true;
                await Task.Delay(2000);
                  
                    await Navigation.PushAsync(new MyPage());
                act.IsRunning = false;
                act.IsVisible = false;
                }
                else{
                    await DisplayAlert("Warning", "Username and password is incorrect", "ok");
                }

            }
            else{
                await DisplayAlert("Warning", "please enter Username and password ", "ok");
            }
        }
    }
}
