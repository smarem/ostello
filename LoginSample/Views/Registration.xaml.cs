﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LoginSample
{
    public partial class Registration : ContentPage
    {


        public Registration()
        {
            InitializeComponent();

            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (s, e) =>
            {
                Navigation.PushAsync(new MainPage());

            };
            login.GestureRecognizers.Add(tap);
        }

        void Handle_DateSelected(object sender, Xamarin.Forms.DateChangedEventArgs e)
        {

        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
         
            if (rusername.Text!=null && rpassword!= null && rrenter.Text!=null && remai.Text!=null){
                if(rpassword.Text==rrenter.Text){
                    SqlHelper sqlHelper = new SqlHelper();
                    RegEntity reg = new RegEntity();
                    reg.Name = remai.Text;
                    reg.Username = rusername.Text;
                    reg.Password = rpassword.Text;
                    int i = sqlHelper.SaveItem(reg);
                    if(i>0){
                        DisplayAlert("Notification", "successfully Registered", "ok");

                        Navigation.PopAsync();

                    }
                    else{
                        DisplayAlert("Registrtion", "Registrtion Fail .. Please try again ", "OK");

                    }




                }
              
                else{
                    DisplayAlert("Warning", "password and re-enter password is not mathed", "ok");
                }
               
            }
            else{
                DisplayAlert("Notification", "Please enter all the fields", "ok");
            }

        }
    }
}
