﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LoginSample.ViewModel;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using LoginSample.SqlHelpers;
using System.IO;
using Plugin.Geolocator;
using Xamarin.Forms.Maps;

namespace LoginSample
{
    public partial class CreateHostel : ContentPage
    {
        Hostel hostel = new Hostel();
        string path;
        string[] items = { "Take Photo", "Choose From Libray", "Cancel" };
        int index=0;
        string base6464String;
         


        public async void Handle_Clicked_1(object sender, System.EventArgs e)
        {

            var actionSheet = await DisplayActionSheet("Select", "Cancel", null, "Camera", "Photos");

            switch (actionSheet)
            {
                case "Cancel":
                    break;
                case "Camera":
                    await CrossMedia.Current.Initialize();
                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await DisplayAlert("No Camera", ":( No camera available.", "OK");
                        return;
                    }
                    var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        Directory = "Sample",
                        Name = "test.jpg"
                    });
                    if (file == null)
                        return;
                    await DisplayAlert("File Location", file.Path, "OK");
                    path = file.Path;
                    hostelimage.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                 break;

                    case "Photos":
                    if (!Plugin.Media.CrossMedia.Current.IsPickPhotoSupported || !Plugin.Media.CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await DisplayAlert("oops", "Pick Photo is not Supported", "OK");
                        return;
                    }

                    var MediaOptions = new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.Medium
                    };
                    var selectedImage = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(MediaOptions);
                     if (selectedImage == null)
                        return;
                    path = selectedImage.Path;
                    hostelimage.Source = ImageSource.FromStream(() => { return selectedImage.GetStream(); });
                    break;
                        }

        }

      
        MyPageViewModel myPageViewModel1;

       
        public CreateHostel(){
            InitializeComponent();
           
        }

        public CreateHostel( MyPageViewModel myPageViewModel1)
        {
            this.myPageViewModel1 = myPageViewModel1;
            InitializeComponent();
            TapGestureRecognizer tap1 = new TapGestureRecognizer();
            tap1.Tapped += (sender, e) => {
                namelabel.IsVisible = false;
            };
            hname.GestureRecognizers.Add(tap1);
        }

        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            namelabel.IsVisible = false;
        }

        public CreateHostel(string name,string number,string image,int index){
            InitializeComponent();
            
            TapGestureRecognizer tap1 = new TapGestureRecognizer();
            create.IsVisible = false;
            update.IsVisible = true;
            tap1.Tapped += (sender, e) => {
                namelabel.IsVisible = false;
            };
            hname.GestureRecognizers.Add(tap1);
            hname.Text = name;
            hnumber.Text = number;
            this.index = index;
            hostel.ID = index;
            base6464String = image;
            byte[] base64toStream = Convert.FromBase64String(image);
            hostelimage.Source = ImageSource.FromStream(() =>
                                                        new MemoryStream(base64toStream));
        }

        void Handle_Clicked_2(object sender, System.EventArgs e)
        {
            MyPageViewModel myPageViewModel = new MyPageViewModel();
            if (path != null){

           
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];
            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            //Close the File Stream
            fs.Close();
            string _base64String = Convert.ToBase64String(ImageData);
          
                myPageViewModel.UpdateHostelList(hname.Text, hnumber.Text,_base64String ,index,hlocat.Text);
            Navigation.PushAsync(new MyPage());
            }else{
                myPageViewModel.UpdateHostelList(hname.Text, hnumber.Text, base6464String, index,hlocat.Text);
                Navigation.PushAsync(new MyPage());
            }

        }

        async  void Handle_Clicked(object sender, System.EventArgs e)

        {
            var b = sender as Button;
            switch(b.Text){
                case "Create":
                        if (hname.Text == null && hnumber.Text == null)
                        {
                            await DisplayAlert("warning", "Please fill All the Details", "ok");
                        }
                        else
                        {
                            // provide read access to the file
                            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                            // Create a byte array of file stream length
                            byte[] ImageData = new byte[fs.Length];
                            //Read block of bytes from stream into the byte array
                            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
                            //Close the File Stream
                            fs.Close();
                            string _base64String = Convert.ToBase64String(ImageData);
                        myPageViewModel1.UpdateHostelList(hname.Text, hnumber.Text, _base64String, index,hlocat.Text);
                            await DisplayAlert("Notification", "successfully Registered", "ok");
                            await Navigation.PopAsync();

                        }
                    break;

                case "Use Current Location":

                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 20;
                    var position = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(10000));
                   // var address = await locator.GetAddressesForPositionAsync(position,null);
                    Geocoder geo = new Geocoder();
                   // var address = await geo.GetAddressesForPositionAsync(position.Latitude,position.Longitude);
                    var positions = new Position(position.Latitude, position.Longitude);
                    var address = await geo.GetAddressesForPositionAsync(positions);

                    foreach (var s in address)
                    {
                        hlocat.Text += s + "\n";
                    }
                    break;
            }


        }

    }
}
