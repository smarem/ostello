﻿using System;
using System.Collections.Generic;
using System.IO;
using LoginSample.SqlHelpers;
using LoginSample.ViewModel;
using Xamarin.Forms;

namespace LoginSample.Views
{
    public partial class ViewHostel : ContentPage
    {
        HostelSqlHelper hostelSQLiteHelper = new HostelSqlHelper();
        Hostel hostel = new Hostel();
        private int index;

        public ViewHostel()
        {
            InitializeComponent();
           
        }
        MyPageViewModel model;

        public ViewHostel(int index, MyPageViewModel myPageViewModel)
        {
            this.index = index;
            InitializeComponent();
            model = myPageViewModel;
            hostel = hostelSQLiteHelper.GetHostel(index);
            hostelmname.Text = hostel.Name;
            hostelnumber.Text = hostel.Number;

            byte[] Base64Stream = Convert.FromBase64String(hostel.Image);
            image.Source = ImageSource.FromStream(() => new MemoryStream(Base64Stream));

         

        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new CreateHostel(hostel.Name, hostel.Number, hostel.Image,index));
        }

        void Handle_Clicked_1(object sender, System.EventArgs e)
        {
            model.DeleteHostel(hostel);
            Navigation.PopAsync();
        }
    }
}
