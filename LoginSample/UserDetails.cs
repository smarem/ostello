﻿using System;

using Xamarin.Forms;

namespace LoginSample
{
    public class UserDetails : ContentPage
    {
        public UserDetails()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

