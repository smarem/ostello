﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using LoginSample.SqlHelpers;
using LoginSample.Views;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace LoginSample.ViewModel
{
    public class MyPageViewModel:BaseViewModel
    {
        public INavigation navigation;
       
        Hostel hostel;
        public HostelSqlHelper hostelSqlHelper = new HostelSqlHelper();
        public MyPageViewModel()
        {


            // HostelList = new ObservableCollection<Hostel>();
            HostelList = new List<Hostel>();
            HostelList = (System.Collections.Generic.List<LoginSample.Hostel>)hostelSqlHelper.GetHostels();

        }

        private List<Hostel> _hostelList;
        public List<Hostel> HostelList 
        {
            set
            {
                _hostelList = value;
                RaisePropertyChanged("HostelList");
            }
            get
            {
                return _hostelList;
            }
        }
      
        public void UpdateHostelList(string hostelName, string hostelNumber,string path,int id,string location)
        {


            Hostel hostel1 = new Hostel { Name = hostelName, Number = hostelNumber, Image = path,ID=id,Location=location };
            var bytes = Convert.FromBase64String(path);
           

            int i = hostelSqlHelper.SaveHostel(hostel1);
            if (i > 0)
            {
                var imageSource = ImageSource.FromStream(() => new MemoryStream(bytes));
                HostelList = (System.Collections.Generic.List<LoginSample.Hostel>)hostelSqlHelper.GetHostels();
            }
        }
        public void DeleteHostel (Hostel i){
          
            hostelSqlHelper.DeleteHostel(i);
            HostelList = (System.Collections.Generic.List<LoginSample.Hostel>)hostelSqlHelper.GetHostels();

        }
        public ICommand OpenMapCommand
        {
            get{
                return new Command<Hostel>(OpenMap);
            }
        }
        void OpenMap(Hostel hostel)
        {
            
            navigation.PushAsync(new MapsPage(hostel.Location));
        }






        public  ICommand createHostelSelected{
            get{
                return new Command(createHostelSelectedCommand);
            }
           
        }
        public void createHostelSelectedCommand(){
            navigation.PushAsync(new CreateHostel(this));
        }
        public ICommand ArrowCommand
        {
            get
            {
                return new Command<Hostel>(ArrowClick);
            }
        }

        void ArrowClick(Hostel obj)
        {
            hostel = obj;
            navigation.PushAsync(new ViewHostel(obj.ID,this));
        }

    }
}
