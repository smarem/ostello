﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using LoginSample.CustomRenderers;
using LoginSample.iOS;
using CoreGraphics;
using CoreLocation;
using MapKit;
using ObjCRuntime;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PolylineMap), typeof(PolylineMapRenderer))]
namespace LoginSample.iOS
{
    public class PolylineMapRenderer : MapRenderer
    {
        MKPolylineRenderer polylineRenderer;
        List<Pin> routePins;

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (null == this.Element || null == this.Control)
                return;
            if (e.PropertyName == PolylineMap.IsCoordinatesAddedProperty.PropertyName)
            {
                UpdatePolyLine((PolylineMap)sender);
            }

            if (e.PropertyName == PolylineMap.IsPinsAddedProperty.PropertyName)
            {
                UpdatePins((PolylineMap)sender);
            }
        }
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                if (nativeMap != null)
                {
                    // Remove paths
                    nativeMap.RemoveOverlays(nativeMap.Overlays);
                    nativeMap.OverlayRenderer = null;
                    polylineRenderer = null;

                    //Remove Pins
                    nativeMap.RemoveAnnotations(nativeMap.Annotations);
                    nativeMap.GetViewForAnnotation = null;
                }
            }

            if (e.NewElement != null)
            {
                var formsMap = (PolylineMap)e.NewElement;

                // Path
                UpdatePolyLine(formsMap);

                //Pins
                UpdatePins(formsMap);
            }
        }

        /// <summary>
        /// Updates the poly line.
        /// </summary>
        /// <param name="map">Map.</param>
        private void UpdatePolyLine(PolylineMap map)
        {
            var nativeMap = Control as MKMapView;

            nativeMap.OverlayRenderer = GetOverlayRenderer;

            CLLocationCoordinate2D[] coords = new CLLocationCoordinate2D[map.RouteCoordinates.Count];

            int index = 0;
            foreach (var position in map.RouteCoordinates)
            {
                coords[index++] = new CLLocationCoordinate2D(position.Latitude, position.Longitude);
            }

            var routeOverlay = MKPolyline.FromCoordinates(coords);
            nativeMap.AddOverlay(routeOverlay);
        }

        //Path
        MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlayWrapper)
        {
            if (polylineRenderer == null && !Equals(overlayWrapper, null))
            {
                var overlay = Runtime.GetNSObject(overlayWrapper.Handle) as IMKOverlay;
                polylineRenderer = new MKPolylineRenderer(overlay as MKPolyline)
                {
                    FillColor = UIColor.Blue,
                    StrokeColor = UIColor.Red,
                    LineWidth = 3,
                    Alpha = 0.4f
                };
            }
            return polylineRenderer;
        }

        /// <summary>
        /// Updates the pins.
        /// </summary>
        /// <param name="map">Map.</param>
        private void UpdatePins(PolylineMap map)
        {
            var nativeMap = Control as MKMapView;
            routePins = map.RoutePins;
            foreach (Pin pin in routePins)
            {
                CLLocationCoordinate2D fromCoords = new CLLocationCoordinate2D(pin.Position.Latitude, pin.Position.Longitude);
                MKPointAnnotation mKPointAnnotation = new MKPointAnnotation();
                mKPointAnnotation.Coordinate = fromCoords;
                mKPointAnnotation.Title = pin.Label;
                mKPointAnnotation.Subtitle = pin.Address;
                nativeMap.AddAnnotation(mKPointAnnotation);
            }
        }
        //Pins/Annotations
        protected override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            annotationView = mapView.DequeueReusableAnnotation(annotation.ToString());
            if (annotationView == null)
            {
                annotationView = new MKAnnotationView(annotation, annotation.ToString());
                annotationView.Image = UIImage.FromFile("Mappin.png");
                annotationView.CalloutOffset = new CGPoint(0, 0);
                annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("DefaultProfilePic.png"));
                //annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
            }
            else
            {
                annotationView.Annotation = annotation;
            }
            annotationView.CanShowCallout = true;

            return annotationView;
        }
    }
}
